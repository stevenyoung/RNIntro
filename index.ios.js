/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {
  AppRegistry,
  Component,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from 'react-native';

class NewProject extends Component {
  // constructor sets up default values for the view
  constructor(props) {
    super(props);
    this.state = {
      movies: null,
      inputText: '',
    };
  }

  // Event Handlers - Code to handle UI events

  // this is triggered when button is pressed
  _onPressButton(event) {
    console.log('button pressed', event)
  }

  // this is triggered when text is changed input box
  textChanged(params) {
    console.log('text changed', params)
    console.log(this.state.inputText)
    this.setState({inputText: params.inputText})
  }

  // this is triggered after this component any loaded
  componentDidMount() {
    console.log('this', this)
    console.log('https://facebook.github.io/react-native/docs/tutorial.html#content')
  }

  // render method shows the component view
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native!
        </Text>
        <Image style={styles.vr}
          source={require('./vr.jpg')}
        />
        <Text style={styles.instructions}>
          Type a search term:
        </Text>
        <TextInput style={styles.textInput}
          value = {this.state.inputText}
          onChangeText = {(inputText) => this.textChanged({inputText})}
        />
        <TouchableOpacity activeOpacity={.5}
          onPress={this._onPressButton(this)}>
          <View style={styles.button}>
            <Text style={styles.buttonText}>Look it up</Text>
          </View>
        </TouchableOpacity>
        <Text style={styles.instructions}>
          Press Cmd+R to reload,{'\n'}
          Cmd+D or shake for dev menu</Text>
        <Text>text: {this.state.inputText} </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    marginTop: 40,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  textInput: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 20,
    marginTop: 20,
    marginRight: 40,
    marginBottom: 20,
    marginLeft: 40,
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: 'white'

  },
  button: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 20,
    padding: 5,
    margin: 5,
    backgroundColor: 'yellow'
  },
  buttonText: {
    lineHeight: 17,
    margin: 5
  },
  vr: {
    flex: 1,
    margin: 10
  }
});

AppRegistry.registerComponent('NewProject', () => NewProject);
